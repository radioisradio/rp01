var 
	fs		= require('fs'),
	sax		= require('sax'),
	util	= require('util');

exports.GetXmlData = function(elements,fileSourse) {
	return XmlParse(elements,fileSourse);
};

function XmlParse(elements,fileSource) {
	elements = elements || [];
		

	var self = this;
	var currentObject;
	var inObject = false;
	var inObjectName;
	var ancestors = [];

	var 
		strict = true, // set to false for html-mode
		parser = sax.parser(strict);

	parser.onerror = function (e) {
	// an error happened.	
	};
	parser.onopentag = function (args) {		
		if(!inObject) {
			// If we are not in an object and not tracking the element
			// then we don't need to do anything
			if (elements.indexOf(args.name) < 0) {
				return;
			}

			// Start tracking a new object
			inObject = true;
			inObjectName = args.name;

			currentObject = {};
		}

		if (!(args.name in currentObject)) {
			currentObject[args.name] = args.attributes;
		} else if (!util.isArray(currentObject[args.name])) {
			// Put the existing object in an array.
			var newArray = [currentObject[args.name]];
			
			// Add the new object to the array.
			newArray.push(args.attributes);
			
			// Point to the new array.
			currentObject[args.name] = newArray;
		} else {
			// An array already exists, push the attributes on to it.
			currentObject[args.name].push(args.attributes);
		}

		// Store the current (old) parent.
		ancestors.push(currentObject);

		// We are now working with this object, so it becomes the current parent.
		if (currentObject[args.name] instanceof Array) {
			// If it is an array, get the last element of the array.
			currentObject = currentObject[args.name][currentObject[args.name].length - 1];
		} else {
			// Otherwise, use the object itself.
			currentObject = currentObject[args.name];
		}
	};

	parser.ontext = function (data) {
	// got some text.  t is the string of text.		
		if(!inObject) {
			return;
		}

		data = data.trim();

		if (!data.length) {
			return;
		}		
		currentObject['$t'] = (currentObject['$t'] || "") + data;			
	};
	

	parser.onclosetag = function (name) {
	// an attribute.  attr has "name" and "value"
		if(!inObject) {
			return;
		}

		if(inObject && inObjectName === name) {
			// Finished building the object
			//self.emit('object', name, currentObject);

			inObject = false;
			ancestors = [];

			return;
		}

		if(ancestors.length) {
			var ancestor = ancestors.pop();
			var keys = Object.keys(currentObject);

			if (keys.length == 1 && '$t' in currentObject) {
				// Convert the text only objects into just the text
				if (ancestor[name] instanceof Array) {
					ancestor[name].push(ancestor[name].pop()['$t']);
				} else {
					ancestor[name] = currentObject['$t'];
				}
			} else if (!keys.length) {
				// Remove empty keys
				delete ancestor[name];
			}

			currentObject = ancestor;
		} else {
			currentObject = {};
		}
	};

	parser.onend = function () {
	// parser stream is done, and ready to have more stuff written to it.
	};

	parser.write(fileSource).close();
	return currentObject;

}