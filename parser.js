

// exports
exports.GetXmlData = function() {
	//DictsLoader();
	XmlParse();
};


 
//globals
arrOutputData			= [];
arrOutputDataForms		= [];
arrOutputDataAddress	= [];
arrDictionaries			= {};
arrDivision				= [];
parserCfg				= {};
iterator				= 0;



function XmlParse()
{

	var
		fs				= require('fs'),
		async			= require('async'),
		cfgFile			= GetFileCfg('parser.cfg.json');

	parserCfg = cfgFile;
	DictsLoader();

	//получаем имена всех файлов в папке
	var arrFilenames		= fs.readdirSync('./data/xml/'),
		arrayLength			= arrFilenames.length,
		arrAsyncContainer	= [],
		j					= 0;

	
	for(var i in arrFilenames) {
		if (j === 500 ) {
			async.series(arrAsyncContainer,function(err,result) {	
				arrAsyncContainer	= [];
				console.log('500');				
			});
			j = 0;
		}
		arrAsyncContainer.push(InitParser(arrFilenames[i],arrayLength));
		j++;

	}
	if (arrAsyncContainer.length > 0) {
		console.log(arrAsyncContainer.length);				
		async.series(arrAsyncContainer,function(err,result) {	
				arrAsyncContainer	= [];
				
		});
	}
	
	
	

}

function GetTerrCode(terrCode)
{
	return terrCode.substr(0,7).replace('-','');
}

function GetTerritoryCodeToDB(terrCode)
{

	var code = GetTerrCode(terrCode);	

	for(var i in arrDictionaries['REG_ORGAN']) {
		for(var j in arrDictionaries['REG_ORGAN'][i]['element']) {

			if (arrDictionaries['REG_ORGAN'][i]['element'][j] === code) {				
				return arrDictionaries['REG_ORGAN'][i]['DB'];	
			}
		
		}
	}
	return '';
}

function GetDocTypeToDB(code)
{
	for(var i in arrDictionaries['DOC_TYPE']) {
		if (arrDictionaries['DOC_TYPE'][i]['element'] === code) {
			return arrDictionaries['DOC_TYPE'][i]['DB'];	
		}
	}
	return '';
}


function GetPassReasonToDB(code)
{
	for(var i in arrDictionaries['REASON']) {
		if (arrDictionaries['REASON'][i]['element'] === code) {
			return arrDictionaries['REASON'][i]['DB'];	
		}
	}
	return '';
}


function DictsLoader()
{
	var
		dict		= require('./dict/dict.js');

	arrDictionaries['REG_ORGAN']	= dict.GetTerritories();
	arrDictionaries['DOC_TYPE']		= dict.GetDocTypes();
	arrDictionaries['REASON']		= dict.GetPassReasons();
	return;
}

function _Get(obj,childs)
{
	if (obj[childs] === undefined) {
		return '';
	}
	else {
		return obj[childs];
	}
}

function _Trim(str)
{
	var
		strOutput = str.replace(/^\s/,'');
		strOutput = strOutput.replace(/\s$/,'');

	return strOutput;
}

function GetSubdivision(xmlSource)
{
	var 
		xml2object		= require('./parse_xml.js'),
		obj				= xml2object.GetXmlData(['subdivision'],xmlSource),		
		boolVal			= false;
	
	for(var it in arrDivision) {
		if (arrDivision[it].element === obj.element) {
			boolVal = true;
			break;
		}			
	}
	if (boolVal === false) {
		arrDivision.push(obj);
		SaveDictionaryData(obj.value + ' ' + obj.element);
	}	
	return;
}

function ParseFormReasons(reason,num)
{
	var boolVal			= false;

	for(var it in arrDivision) {
		if (arrDivision[it] === reason) {
			boolVal = true;
			break;
		}			
	}
	if (boolVal === false) {
		arrDivision.push(reason);
		SaveDictionaryData(reason+' '+num);
	}	
	return;
}

function GetFileContent(filename)
{
	var		
		fs			= require('fs'),
		fileData	= fs.readFileSync('./data/xml/'+filename,{'encoding':'utf8'});
	
	return fileData;	
}

function GetFileCfg(filename)
{
	var		
		fs			= require('fs'),
		fileData	= fs.readFileSync(filename,{'encoding':'utf8'});

	return JSON.parse(fileData);	
}


function InitParser(filename,arrLength)
{	 
	return function(callback)
	{
		var
		pattern			= /^([a-zA-Z]*)\_*?/,
		resOutput		= pattern.exec(filename),		
		xml2object		= require('./parse_xml.js'),
		xmlSource,
		obj,			
		parser;							

		if (resOutput[1]) {

			/**** ***/
			/*
			xmlSource		= GetFileContent(filename);									
			GetSubdivision(xmlSource);		
			
			/**** ***/

			if (resOutput[1] === 'RegByLiving') {
				
				xmlSource		= GetFileContent(filename);									
				obj				= xml2object.GetXmlData([parserCfg.REG_BY_LIVING.node],xmlSource);				

				if (obj) {					
					try {								
						ParseRegByliving(obj);								
					}
					catch(e) {
												
						arrOutputDataForms.push('');
						arrOutputDataAddress.push('');
						WriteErrorData(filename,e);						
					}									
				}
				else {
					WriteErrorData(filename,'not found');
				}				
			}		
				
			else if (resOutput[1] === 'RegByStaying') {
											
				xmlSource		= GetFileContent(filename);
				obj				= xml2object.GetXmlData([parserCfg.REG_BY_STAYING.node],xmlSource);				
					
				if (obj) {
					try {								
						ParseRegByStaying(obj);
					}
					catch(e) {
							arrOutputDataForms.push('');
							arrOutputDataAddress.push('');
							WriteErrorData(filename,e);
					}									
				}
				else {
						WriteErrorData(filename,'not found');
				}					
			}

			else if (resOutput[1] === 'UnregByLiving') {
										
				xmlSource		= GetFileContent(filename);
				obj				= xml2object.GetXmlData([parserCfg.UNREG_BY_LIVING.node],xmlSource);		
					
				if (obj) {
					try {					
						ParseUnregByliving(obj);
					}
					catch(e) {
							arrOutputDataForms.push('');
							arrOutputDataAddress.push('');
							WriteErrorData(filename,e);
					}									
				}
				else {
						WriteErrorData(filename,'not found');
				}								
			}

			else if (resOutput[1] === 'UnregByStaying') {
									
				xmlSource		= GetFileContent(filename);
				obj				= xml2object.GetXmlData([parserCfg.UNREG_BY_STAYING.node],xmlSource);		
					
				if (obj) {
					try {								
						ParseUnregByStaying(obj);
					}
					catch(e) {
							arrOutputDataForms.push('');
							arrOutputDataAddress.push('');
							WriteErrorData(filename,e);
					}									
				}
				else {
						WriteErrorData(filename,'not found');
				}													
			}

			else if (resOutput[1] === 'RussianPassportIssuance') {
						
				xmlSource		= GetFileContent(filename);
				obj				= xml2object.GetXmlData([parserCfg.RUSSIAN_PASSPORT.node],xmlSource);		
					
				if (obj) {
					try {								
						ParseRussianPassport(obj);
					}
					catch(e) {
							arrOutputDataForms.push('');
							arrOutputDataAddress.push('');
							WriteErrorData(filename,e);
					}									
				}
				else {
						WriteErrorData(filename,'not found');
				}								
			}
		}

		else {
			arrOutputDataForms.push('');
		}	
		callback();	
	};	
}


function ParseAddresses(strData,arrHousing)
{	
	var			
		pattern1		= /^обл.([а-яА-Я]*)\$*?/,
		resObl			= pattern1.exec(strData),
		arrOutput		= {},
		arrResults		= [];

	arrOutput.ADR_OBL			= '';
	arrOutput.ADR_RAION			= '';
	arrOutput.ADR_CITY			= '';
	arrOutput.ADR_CITY_TYPE		= '';
	arrOutput.ADR_STREET		= '';
	arrOutput.ADR_STREET_PR		= '';
	arrOutput.ADR_HOUSE			= '';
	arrOutput.ADR_HOUSE_LITERA	= '';
	arrOutput.ADR_FLAT			= '';			

	strData += '$';
	strData = strData.replace(/\$/g,' $');

	var 
		defaultPattern	= /([а-я\-]*?)\s([а-я\sА-Я\-0-9\.\(\)\№]*)\$/g,		
		result			= defaultPattern.exec(strData);	
		it				= 1,
		arrResults[0]	= result;
	
	while(result) {
		result			= defaultPattern.exec(strData);	
		arrResults[it]	= result;				
		it++;
	}		

	if (arrResults[0]) {
		arrOutput.ADR_OBL = _Trim(arrResults[0][2]);	
	}
	
	if (arrResults[1]) {
		arrOutput.ADR_RAION		= _Trim(arrResults[1][2]);			
	}

	if ( arrResults[2] && _Trim(arrResults[2][1]) !== '' && arrResults[3] && _Trim(arrResults[3][1]) !== '' ) {
		arrOutput.ADR_RAION 	= _Trim(arrResults[2][2]);
		arrOutput.ADR_CITY		= _Trim(arrResults[3][2]);	
		arrOutput.ADR_CITY_TYPE = _Trim(arrResults[3][1]); 	
	}

	else if (arrResults[2] && _Trim(arrResults[2][1]) !== '') {

		arrOutput.ADR_CITY		= _Trim(arrResults[2][2]);	
		arrOutput.ADR_CITY_TYPE = _Trim(arrResults[2][1]);
	}

	else if (arrResults[3] && _Trim(arrResults[3][1]) !== '' ) {
		arrOutput.ADR_CITY		= _Trim(arrResults[3][2]);	
		arrOutput.ADR_CITY_TYPE = _Trim(arrResults[3][1]);
	}

	if (arrResults[5]) {
		arrOutput.ADR_STREET	= _Trim(arrResults[5][2]);				
		arrOutput.ADR_STREET_PR	= _Trim(arrResults[5][1]);	
	}	

	if (arrHousing !== undefined)
	{
		if (arrHousing.type !== undefined)
		{
			if (arrHousing.type.value === 'Дом')
			{
				arrOutput.ADR_HOUSE = arrHousing.value;			
			}
		}
		else
		{
			for(var i in arrHousing)
			{								
				if (arrHousing[i].type !== undefined)
				{
					if (arrHousing[i].type.value === 'Дом' )
					{
						arrOutput.ADR_HOUSE = arrHousing[i].value;			
					}

					else if (arrHousing[i].type.value === 'Корпус' )
					{
						arrOutput.ADR_HOUSE_LITERA = arrHousing[i].value;									
					}

					else if (arrHousing[i].type.value === 'Квартира' )
					{
						arrOutput.ADR_FLAT = arrHousing[i].value;
					}
					else if (arrHousing[i].type.value === 'Комната' )
					{
						arrOutput.ADR_FLAT +=' комн. '+ arrHousing[i].value;									
					}
				}				
			}
		}
		
	}	

	return arrOutput;

}

function ParseBirthAddresses(person)
{
	var 
		arrOutput		= {},
		arrResults		= [];

	arrOutput.COUNTRY	= '';
	arrOutput.REGION	= '';
	arrOutput.RAION		= '';
	arrOutput.CITY		= '';
	arrOutput.CITY_TYPE	= '';
	arrOutput.ALL		= '';
	

	arrOutput.COUNTRY	= person.birthPlace.country.value;
	var
		pattern			= /([а-я\sА-Я\-\.\(\)\№]*)\s([а-я\sА-Я\-0-9\.\(\)\№]+)/,
		arrRes			= pattern.exec(_Get(person['birthPlace'],'place'));

	if (arrRes !== null) {		
		if (arrRes[2] !== 'обл.' && arrRes[2] !== 'ОБЛ.' && arrRes[2] !== 'КРАЯ' && arrRes[2] !== 'края' && arrRes[2] !== 'КРАЙ' && arrRes[2] !== 'ОБЛАСТЬ') {
			arrOutput.REGION = _Get(person['birthPlace'],'place');	
		}
		else {
			arrOutput.REGION = arrRes[1];	
		}
	
	};
		
	
	if (_Get(person['birthPlace'],'place2') !== '') {
		pattern			= /([а-я\sА-Я\-\.\(\)\№]*)\s([а-я\sА-Я\-0-9\.\(\)\№]+)/;
		arrRes			= pattern.exec(_Get(person['birthPlace'],'place2'));

		if (arrRes !== null) {
			if (arrRes[2] !== 'Р-Н' && arrRes[2] !== 'р-н' && arrRes[2] !== 'р-на' && arrRes[2] !== 'Р-НА' && arrRes[2] !== 'РАЙОНА' && arrRes[2] !== 'РАЙОН' && arrRes[2] !== 'района' && arrRes[2] !== 'район') {
			arrOutput.RAION		= _Get(person['birthPlace'],'place2');
			}
			else {
				arrOutput.RAION		= arrRes[1];
			}	
		}
		else {
			arrOutput.RAION		= _Get(person['birthPlace'],'place2');  
		}
	}
	
	if (_Get(person['birthPlace'],'place3') !== '' && _Get(person['birthPlace'],'place4') == '') {

		pattern			= /([а-яА-Я\-\.\(\)\№]*?)\s([а-я\sА-Я0-9\-\.\(\)\№]+)/;
		arrRes			= pattern.exec(_Get(person['birthPlace'],'place3'));
		if (arrRes !== null) {
				if	(arrRes[1] !== 'Г.' && arrRes[1] !== 'г' && arrRes[1] !== 'С.' && arrRes[1] !== 'с' && arrRes[1] !== 'ПОС.' && arrRes[1] !== 'п' && arrRes[1] !== 'П.' && arrRes[1] !== 'ГОР.' && arrRes[1] !== 'Р.П.' && arrRes[1] !== 'рп' && arrRes[1] !== 'ПГТ.' && arrRes[1] !== 'пгт' && arrRes[1] !== 'д' && arrRes[1] !== 'Д.' && arrRes[1] !== 'ДЕР.' && arrRes[1] !== 'ст' && arrRes[1] !== 'СТ.' && arrRes[1] !== 'гор.' && arrRes[1] !== 'ГО.' && arrRes[1] !== 'ГОРО.' && arrRes[1] !== 'СЕЛО' && arrRes[1] !== 'РП' && arrRes[1] !== 'ст-ца' && arrRes[1] !== 'СТАНЦИЯ') { 
					arrOutput.CITY		= _Get(person['birthPlace'],'place3')
				}
				else {
					arrOutput.CITY		= arrRes[2];
					arrOutput.CITY_TYPE	= arrRes[1];
				}
			}
	
		else {
			arrOutput.CITY		= _Get(person['birthPlace'],'place3');
			}
		}
	else if (_Get(person['birthPlace'],'place4') !== '') {
		pattern			= /([а-яА-Я\-\.\(\)\№]*?)\s([а-я\sА-Я0-9\-\.\(\)\№]+)/;
		arrRes			= pattern.exec(_Get(person['birthPlace'],'place4'));
		if (arrRes !== null) {
				if	(arrRes[1] !== 'Г.' && arrRes[1] !== 'г' && arrRes[1] !== 'С.' && arrRes[1] !== 'с' && arrRes[1] !== 'ПОС.' && arrRes[1] !== 'п' && arrRes[1] !== 'П.' && arrRes[1] !== 'ГОР.' && arrRes[1] !== 'Р.П.' && arrRes[1] !== 'рп' && arrRes[1] !== 'ПГТ.' && arrRes[1] !== 'пгт' && arrRes[1] !== 'д' && arrRes[1] !== 'Д.' && arrRes[1] !== 'ДЕР.' && arrRes[1] !== 'ст' && arrRes[1] !== 'СТ.' && arrRes[1] !== 'гор.' && arrRes[1] !== 'ГО.' && arrRes[1] !== 'ГОРО.' && arrRes[1] !== 'СЕЛО' && arrRes[1] !== 'РП' && arrRes[1] !== 'ст-ца' && arrRes[1] !== 'СТАНЦИЯ') { 
					arrOutput.CITY		= _Get(person['birthPlace'],'place4')
				}
				else {
					arrOutput.CITY		= arrRes[2];
					arrOutput.CITY_TYPE	= arrRes[1];
				}
			}
		else {
			arrOutput.CITY		= _Get(person['birthPlace'],'place4');
		}
	}

	arrOutput.ALL = _Get(person['birthPlace'],'place') + ' ' + _Get(person['birthPlace'],'place2') + ' ' + _Get(person['birthPlace'],'place3') + ' ' + _Get(person['birthPlace'],'place4');
	
	return arrOutput;

}


function _GetRealPath(obj,strPath) {

	var
		blank		= /([a-zA-Z0-9:]{1,})/g,
		arrRes		= strPath.match(blank),
		arrOutput	= '';	

	if (arrRes) {
		arrOutput	= obj;
		for(var i in arrRes) {
			if (arrOutput[arrRes[i]]) {
				arrOutput = arrOutput[arrRes[i]];		
			}
			else {
				return '';
			}
				
		}	
	}	
	 
	return arrOutput;

}


function ParseRegByliving(obj)
{		
	var
		subdivision			= GetTerritoryCodeToDB(obj[parserCfg.REG_BY_LIVING.subdivision]),
		listID				= obj[parserCfg.REG_BY_LIVING.listID],
		employer			= obj[parserCfg.REG_BY_LIVING.employer],
		person				= _GetRealPath(obj,parserCfg.REG_BY_LIVING.person),		
		documents			= _GetRealPath(obj,parserCfg.REG_BY_LIVING.documents),
		prevAddress			= _GetRealPath(obj,parserCfg.REG_BY_LIVING.prevAddress),

		livAddress			= _GetRealPath(obj,parserCfg.REG_BY_LIVING.livAddress),	
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '',
		boolVal				= false;

		//проверка для подгрузки

		if (_GetRealPath(obj,parserCfg.REG_BY_LIVING.short_form) !== '' && _GetRealPath(obj,parserCfg.REG_BY_LIVING.short_form_adm) === '') {			
			WriteErrorData(listID,'Краткая форма не обработана');
			return false;
			
		}


	if (livAddress !== '') {
		var 
			adrAll				= livAddress.addressObject.value;
			adrHouseFlat		= livAddress.housing;
	}
	else {
		var 
			adrAll				= '';
			adrHouseFlat		= '';
	} 


		if (prevAddress !== '')
		{		
			if (prevAddress.address.russianAddress !== undefined) {
				livAddressPrev		= prevAddress.address.russianAddress;
				adrAllPrev			= livAddressPrev.addressObject.value;
				adrHouseFlatPrev	= livAddressPrev.housing;		
			}	
			
		}		
				
		var 
			arrPerson			= {},
			strForm				= '',
			strAddress			= '';				


		arrPerson.LASTNAME		= person.lastName;
		arrPerson.FIRSTNAME		= person.firstName;
		arrPerson.MIDDLENAME	= _Get(person,'middleName');
		arrPerson.BIRTH			= person.birthDate;
		arrPerson.GR			= person.citizenship.value;

		if (person.gender.element === 'M') {
			arrPerson.SEX = 1;	
		}
		else {
			arrPerson.SEX = 2;
		}
		
		arrPerson.REGISTRATION_TYPE = '1';
		arrPerson.REGISTERED		= '';	
		arrPerson.ORGAN				= subdivision;

		arrPerson.DOC_TYPE		= GetDocTypeToDB(_Get(documents,'type').element);			
		arrPerson.DOC_SERIES	= _Get(documents,'series');		
		arrPerson.DOC_NUM		= _Get(documents,'number');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan').value;		
		if (arrPerson.DOC_ORGAN === undefined) {
			arrPerson.DOC_ORGAN		= _Get(documents,'authority');		
		}
		
		arrPerson.DOC_DATE		= _Get(documents,'issued');		
		arrPerson.CHANGES		= '';				

		/*
		<ns22:decisionCause>
            <type>reg.decisionBasis</type>
            <element>2076</element>
            <value>П.18 Правил (Общие основания)</value> 
        </ns22:decisionCause>
        */	

		//arrPerson.OTHER			= _Get(_Get(obj['ns22:decision'],'ns22:decisionCause'),'value');
		arrPerson.OTHER			= _GetRealPath(obj,parserCfg.REG_BY_LIVING.other);		

		//2   ПЕРЕЕЗД ПРИБЫТИЕ В ТОМ ЖЕ НАСЕЛЕННОМ ПУНКТЕ
		arrPerson.REASON		= '42';	

		if (_GetRealPath(obj,parserCfg.REG_BY_LIVING.creation_date) !== '') {
			arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.REG_BY_LIVING.creation_date);		
			arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.REG_BY_LIVING.check_date);
		}
		else {
			arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.REG_BY_LIVING.alter_date);		
			arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.REG_BY_LIVING.alter_date);		
		}
		

		arrPerson.PREV_FAM				= '';
		arrPerson.PREV_NAME				= '';
		arrPerson.PREV_OTCH				= '';
		arrPerson.PREV_BIRTHDAY			= '';
		arrPerson.SYS_IS_REPLICATION	= '';
		arrPerson.DEATH_NUM				= '';
		arrPerson.DEATH_DATE			= '';
		arrPerson.REGOPER				= _Get(employer,'name');

		if (_GetRealPath(obj,parserCfg.REG_BY_LIVING.livAddressAll) !== '') {
			arrPerson.RPADR					= _Get(_GetRealPath(obj,parserCfg.REG_BY_LIVING.livAddressAll),'dateFrom');			
			arrPerson.RPADRD				= arrPerson.RPADR.substr(8,2);
			arrPerson.RPADRM				= arrPerson.RPADR.substr(5,2);
			arrPerson.RPADRY				= arrPerson.RPADR.substr(0,4);
		}
		else {
			arrPerson.RPADRD				= '';
			arrPerson.RPADRM				= '';
			arrPerson.RPADRY				= '';	
		}

		arrPerson.SPOTM					= '';
		arrPerson.D_VVOD				= _GetRealPath(obj,parserCfg.REG_BY_LIVING.d_vvod);
		
		strForm += subdivision + '00' + listID + '|' + arrPerson.REGISTRATION_TYPE + '|' + arrPerson.LASTNAME + '|' + arrPerson.FIRSTNAME + '|' + arrPerson.MIDDLENAME + '|' + arrPerson.BIRTH + '|' + arrPerson.GR + '|' + '|' + arrPerson.SEX + '|' + '|' + arrPerson.REGISTERED + '|' + arrPerson.ORGAN + '|' ;
		strForm += arrPerson.DOC_TYPE + '|' + arrPerson.DOC_SERIES + '|' + arrPerson.DOC_NUM + '|' + arrPerson.DOC_ORGAN + '|' + arrPerson.DOC_DATE + '|' + arrPerson.CHANGES +'|'+ arrPerson.OTHER +'|' + arrPerson.REASON + '|' + arrPerson.CREATION_DATE + '|';
		strForm += arrPerson.CHECK_DATE + '|' + arrPerson.PREV_FAM + '|' + arrPerson.PREV_NAME + '|' + arrPerson.PREV_OTCH + '|' + arrPerson.PREV_BIRTHDAY + '|' + arrPerson.SYS_IS_REPLICATION + '|' + arrPerson.DEATH_NUM + '|' + arrPerson.DEATH_DATE + '|' + arrPerson.REGOPER + '|' + arrPerson.RPADRD + '|' + arrPerson.RPADRM + '|' + arrPerson.RPADRY + '|' + arrPerson.SPOTM + '|' + arrPerson.D_VVOD + '|' ;
		

		
		
		/*** 3 ***/

		var
			arrAdrBirthplace =  ParseBirthAddresses(person);
		//strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.REGION  +'|' + arrAdrBirthplace.RAION  +'|' + '|'+ arrAdrBirthplace.CITY_TYPE +'|' + arrAdrBirthplace.CITY + '|' + '|' + '||||0|' + "\r\n";
		strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.ALL  +'|' + ''  +'|' + '|'+ '' +'|' + '' + '|' + '|' + '||||0|' + "\r\n";

		//нынешнее метсо жительства

		var 
			arrAdr		= ParseAddresses(adrAll,adrHouseFlat),
			arrAdrPrev	= ParseAddresses(adrAllPrev,adrHouseFlatPrev);

		strAddress += listID + '2'+ '|'+subdivision + '00' + listID + '|' + '1' + '|' +'РОССИЯ'+ '|' + arrAdr.ADR_OBL + '|' + arrAdr.ADR_RAION + '|' + '|' + arrAdr.ADR_CITY_TYPE + '|' + arrAdr.ADR_CITY + '|' + arrAdr.ADR_STREET +' ' + arrAdr.ADR_STREET_PR + '.|' + arrAdr.ADR_HOUSE + '|' + arrAdr.ADR_HOUSE_LITERA + '|' + arrAdr.ADR_FLAT + '|' + '|' + "\r\n";
		//strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +'РОССИЯ'+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';		
		strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +''+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';		

		//вставляем значение в массив форм
		arrOutputDataForms.push(strForm);

		//вставляем значение в массив адресов
		arrOutputDataAddress.push(strAddress);	

		WriteStrIntoFileData();
		return true;	

}



function ParseUnregByliving(obj)
{		
	
	var
		/*
		subdivision			= GetTerritoryCodeToDB(obj.number),
		listID				= obj.uid,
		employer			= obj.employee,
		person				= obj['ns22:declarant'].person,		
		documents			= obj['ns22:declarant'].document,
		prevAddress			= obj['ns22:newAddress'],
		*/
		
		subdivision			= GetTerritoryCodeToDB(obj[parserCfg.UNREG_BY_LIVING.subdivision]),
		listID				= obj[parserCfg.UNREG_BY_LIVING.listID],
		employer			= obj[parserCfg.UNREG_BY_LIVING.employer],
		person				= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.person),		
		documents			= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.documents),
		prevAddress			= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.prevAddress),
		
		livAddress			= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.livAddress),		
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '';

		/*
		livAddress			= obj['ns26:livingRegistrationData'].address.russianAddress,
		adrAll				= livAddress.addressObject.value,
		adrHouseFlat		= livAddress.housing,		
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '';
		*/


		if (livAddress !== '') {
		var 
			adrAll				= livAddress.addressObject.value;
			adrHouseFlat		= livAddress.housing;
		}
		else {
			var 
				adrAll				= '';
				adrHouseFlat		= '';
		} 


		if (prevAddress !== '') {
			//livAddressPrev		= _Get(_Get(obj['ns22:newAddress'],'ns22:address'),'russianAddress');
			livAddressPrev		= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.livAddressPrev);
			if(livAddressPrev !== '') {
				adrAllPrev			= livAddressPrev.addressObject.value;
				adrHouseFlatPrev	= livAddressPrev.housing;		
			}
			
		}		
		
		var 
			arrPerson			= {},
			strForm				= '',
			strAddress			= '';		


		arrPerson.LASTNAME		= person.lastName;
		arrPerson.FIRSTNAME		= person.firstName;
		arrPerson.MIDDLENAME	= _Get(person,'middleName');
		arrPerson.BIRTH			= person.birthDate;
		arrPerson.GR			= person.citizenship.value;


		if (person.gender.element === 'M') {
			arrPerson.SEX = 1;	
		}
		else {
			arrPerson.SEX = 2;
		}

		arrPerson.REGISTRATION_TYPE		= '2';
		arrPerson.REGISTERED			= '';	
		arrPerson.ORGAN					= subdivision;

		arrPerson.DOC_TYPE		= GetDocTypeToDB(_Get(documents,'type').element);	
		arrPerson.DOC_SERIES	= _Get(documents,'series');		
		arrPerson.DOC_NUM		= _Get(documents,'number');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan').value;
		if (arrPerson.DOC_ORGAN === undefined) {
			arrPerson.DOC_ORGAN		= _Get(documents,'authority');		
		}		
		arrPerson.DOC_DATE		= _Get(documents,'issued');		
		arrPerson.CHANGES		= '';	

		/*
		<ns22:deregistrationCause>
			<type>reg.deregistrationCause</type>
			<element>2070</element>
			<value>Смерть или объявление решением суда умершим (основание - свидетельство о смерти, оформленное в установленном порядке)</value>
		</ns22:deregistrationCause>

		*/
		
		//arrPerson.OTHER			= _Get(_Get(obj,'ns22:deregistrationCause'),'value');	
		arrPerson.OTHER			= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.other);			
		arrPerson.OTHER			= arrPerson.OTHER.substr(0,20);		
		arrPerson.REASON		= GetPassReasonToDB(_GetRealPath(obj,parserCfg.UNREG_BY_LIVING.reason));	
		
		
		if (arrPerson.REASON === '') {
			arrPerson.REASON = '43';
		}
		
		//11 - смерть, 23 - призыв
		if (arrPerson.REASON === '11' || arrPerson.REASON === '23' || arrPerson.REASON === '43' ) {
			//Дата решения
			//Номер документа
			//Орган
			var
				freeform_date 	=  _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.freeform_date),
				freeform_num 	=  _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.freeform_num),
				freeform_divsn 	=  _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.freeform_divsn);
			
			if (freeform_date) {
				arrPerson.OTHER += ',' + 'Дата: ' + freeform_date;
			}
			if (freeform_num) {
				arrPerson.OTHER += ',' + 'Номер: ' + freeform_num;
			}
			if (freeform_divsn) {
				arrPerson.OTHER += ',' + 'Орган: ' + freeform_divsn;
			}
		}
			
		
		
		if (_GetRealPath(obj,parserCfg.UNREG_BY_LIVING.creation_date) !== '') {
			arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.creation_date);	
			arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.creation_date);
		}
		else {
			arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.alter_date);		
			arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.alter_date);		
		}

		arrPerson.PREV_FAM				= '';
		arrPerson.PREV_NAME				= '';
		arrPerson.PREV_OTCH				= '';
		arrPerson.PREV_BIRTHDAY			= '';
		arrPerson.SYS_IS_REPLICATION	= '';
		arrPerson.DEATH_NUM				= '';
		arrPerson.DEATH_DATE			= '';
		arrPerson.REGOPER				= _Get(employer,'name');

		if (_GetRealPath(obj,parserCfg.UNREG_BY_LIVING.livAddressAll) !== '') {
			arrPerson.RPADR					= _Get(_GetRealPath(obj,parserCfg.UNREG_BY_LIVING.livAddressAll),'dateFrom');			
			arrPerson.RPADRD				= arrPerson.RPADR.substr(8,2);
			arrPerson.RPADRM				= arrPerson.RPADR.substr(5,2);
			arrPerson.RPADRY				= arrPerson.RPADR.substr(0,4);
		}
		else{
			arrPerson.RPADRD				= '';
			arrPerson.RPADRM				= '';
			arrPerson.RPADRY				= '';	
		}

		arrPerson.SPOTM					= '';
		arrPerson.D_VVOD				= _GetRealPath(obj,parserCfg.UNREG_BY_LIVING.d_vvod);

	
		strForm += subdivision + '00' + listID + '|' + arrPerson.REGISTRATION_TYPE + '|' + arrPerson.LASTNAME + '|' + arrPerson.FIRSTNAME + '|' + arrPerson.MIDDLENAME + '|' + arrPerson.BIRTH + '|' + arrPerson.GR + '|' + '|' + arrPerson.SEX + '|' + '|' + arrPerson.REGISTERED + '|' + arrPerson.ORGAN + '|' ;
		strForm += arrPerson.DOC_TYPE + '|' + arrPerson.DOC_SERIES + '|' + arrPerson.DOC_NUM + '|' + arrPerson.DOC_ORGAN + '|' + arrPerson.DOC_DATE + '|' + arrPerson.CHANGES +'|'+ arrPerson.OTHER +'|' + arrPerson.REASON + '|' + arrPerson.CREATION_DATE + '|';
		strForm += arrPerson.CHECK_DATE + '|' + arrPerson.PREV_FAM + '|' + arrPerson.PREV_NAME + '|' + arrPerson.PREV_OTCH + '|' + arrPerson.PREV_BIRTHDAY + '|' + arrPerson.SYS_IS_REPLICATION + '|' + arrPerson.DEATH_NUM + '|' + arrPerson.DEATH_DATE + '|' + arrPerson.REGOPER + '|' + arrPerson.RPADRD + '|' + arrPerson.RPADRM + '|' + arrPerson.RPADRY + '|' + arrPerson.SPOTM + '|' + arrPerson.D_VVOD + '|' ;


		/*** 3 ***/
		
		var 
			arrAdrBirthplace =  ParseBirthAddresses(person);
		//strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.REGION  +'|' + arrAdrBirthplace.RAION  +'|' + '|'+ arrAdrBirthplace.CITY_TYPE +'|' + arrAdrBirthplace.CITY + '|' + '|' + '||||0|' + "\r\n";
		strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.ALL  +'|' + ''  +'|' + '|'+ '' +'|' + '' + '|' + '|' + '||||0|' + "\r\n";
		//нынешнее метсо жительства
		var 
			arrAdr		= ParseAddresses(adrAll,adrHouseFlat),
			arrAdrPrev	= ParseAddresses(adrAllPrev,adrHouseFlatPrev);

		strAddress += listID + '2'+ '|'+subdivision + '00' + listID + '|' + '1' + '|' +'РОССИЯ'+ '|' + arrAdr.ADR_OBL + '|' + arrAdr.ADR_RAION + '|' + '|' + arrAdr.ADR_CITY_TYPE + '|' + arrAdr.ADR_CITY + '|' + arrAdr.ADR_STREET +' ' + arrAdr.ADR_STREET_PR + '.|' + arrAdr.ADR_HOUSE + '|' + arrAdr.ADR_HOUSE_LITERA + '|' + arrAdr.ADR_FLAT + '|' + '|' + "\r\n";
		//strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +'РОССИЯ'+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +''+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		

		//вставляем значение в массив форм
		arrOutputDataForms.push(strForm);

		//вставляем значение в массив адресов		
		arrOutputDataAddress.push(strAddress);	

		WriteStrIntoFileData();
		return true;		

}



function ParseRegByStaying(obj)
{

	var
		/*
		subdivision			= GetTerritoryCodeToDB(obj.number),
		listID				= obj.uid,
		employer			= obj.employee,
		person				= obj['ns22:declarant'].person,		
		documents			= obj['ns22:declarant'].document,
		prevAddress			= obj['ns22:previousRegistrationAddress'],
		*/

		subdivision			= GetTerritoryCodeToDB(obj[parserCfg.REG_BY_STAYING.subdivision]),
		listID				= obj[parserCfg.REG_BY_STAYING.listID],
		employer			= obj[parserCfg.REG_BY_STAYING.employer],
		person				= _GetRealPath(obj,parserCfg.REG_BY_STAYING.person),		
		documents			= _GetRealPath(obj,parserCfg.REG_BY_STAYING.documents),
		prevAddress			= _GetRealPath(obj,parserCfg.REG_BY_STAYING.prevAddress),

		livAddress			= _GetRealPath(obj,parserCfg.REG_BY_STAYING.livAddress),		
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '';

		if (_GetRealPath(obj,parserCfg.REG_BY_STAYING.short_form) !== '' && _GetRealPath(obj,parserCfg.REG_BY_STAYING.short_form_adm) === '') {			
			WriteErrorData(listID,'Краткая форма не обработана');
			return false;
		}

		if (livAddress !== '') {
			var 
				adrAll				= livAddress.addressObject.value;
				adrHouseFlat		= livAddress.housing;
		}
		else {
			var 
				adrAll				= '';
				adrHouseFlat		= '';
		} 

		if (prevAddress !== '') {
			livAddressPrev		= prevAddress.address.russianAddress;
			adrAllPrev			= livAddressPrev.addressObject.value;
			adrHouseFlatPrev	= livAddressPrev.housing;	
		}		
		
		var 
			arrPerson			= {},
			strForm				= '',
			strAddress			= '';		


		arrPerson.LASTNAME		= person.lastName;
		arrPerson.FIRSTNAME		= person.firstName;
		arrPerson.MIDDLENAME	= _Get(person,'middleName');
		arrPerson.BIRTH			= person.birthDate;
		arrPerson.GR			= person.citizenship.value;

		if (person.gender.element === 'M') {
			arrPerson.SEX = 1;	
		}
		else {
			arrPerson.SEX = 2;
		}
		
		arrPerson.REGISTRATION_TYPE = '1';
		arrPerson.REGISTERED		= _GetRealPath(obj,parserCfg.REG_BY_STAYING.registered);	
		arrPerson.ORGAN				= subdivision;

		arrPerson.DOC_TYPE		= GetDocTypeToDB(_Get(documents,'type').element);	
		arrPerson.DOC_SERIES	= _Get(documents,'series');		
		arrPerson.DOC_NUM		= _Get(documents,'number');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan').value;
		if (arrPerson.DOC_ORGAN === undefined) {
			arrPerson.DOC_ORGAN		= _Get(documents,'authority');		
		}		
		arrPerson.DOC_DATE		= _Get(documents,'issued');		
		arrPerson.CHANGES		= '';		
		arrPerson.OTHER			= _GetRealPath(obj,parserCfg.REG_BY_STAYING.other);		

		//38  РЕГИСТРАЦИЯ ПО МЕСТУ ПРЕБЫВАНИЯ
		arrPerson.REASON		= '38';				
		//arrPerson.CREATION_DATE = _Get(obj['ns24:stayingRegistrationData'],'dateFrom');	
		//arrPerson.CHECK_DATE	= _Get(obj,'date');
		arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.REG_BY_STAYING.creation_date);	
		arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.REG_BY_STAYING.check_date);

		arrPerson.PREV_FAM				= '';
		arrPerson.PREV_NAME				= '';
		arrPerson.PREV_OTCH				= '';
		arrPerson.PREV_BIRTHDAY			= '';
		arrPerson.SYS_IS_REPLICATION	= '';
		arrPerson.DEATH_NUM				= '';
		arrPerson.DEATH_DATE			= '';
		arrPerson.REGOPER				= _Get(employer,'name');

		if (_GetRealPath(obj,parserCfg.REG_BY_STAYING.livAddressAll) !== '') {
			arrPerson.RPADR					= _Get(_GetRealPath(obj,parserCfg.REG_BY_STAYING.livAddressAll),'dateFrom');			
			arrPerson.RPADRD				= arrPerson.RPADR.substr(8,2);
			arrPerson.RPADRM				= arrPerson.RPADR.substr(5,2);
			arrPerson.RPADRY				= arrPerson.RPADR.substr(0,4);
		}
		else {
			arrPerson.RPADRD				= '';
			arrPerson.RPADRM				= '';
			arrPerson.RPADRY				= '';	
		}

		arrPerson.SPOTM					= '';
		arrPerson.D_VVOD				= _GetRealPath(obj,parserCfg.REG_BY_STAYING.d_vvod);

		
		strForm += subdivision + '00' + listID + '|' + arrPerson.REGISTRATION_TYPE + '|' + arrPerson.LASTNAME + '|' + arrPerson.FIRSTNAME + '|' + arrPerson.MIDDLENAME + '|' + arrPerson.BIRTH + '|' + arrPerson.GR + '|' + '|' + arrPerson.SEX + '|' + '|' + arrPerson.REGISTERED + '|' + arrPerson.ORGAN + '|' ;
		strForm += arrPerson.DOC_TYPE + '|' + arrPerson.DOC_SERIES + '|' + arrPerson.DOC_NUM + '|' + arrPerson.DOC_ORGAN + '|' + arrPerson.DOC_DATE + '|' + arrPerson.CHANGES +'|'+ arrPerson.OTHER +'|' + arrPerson.REASON + '|' + arrPerson.CREATION_DATE + '|';
		strForm += arrPerson.CHECK_DATE + '|' + arrPerson.PREV_FAM + '|' + arrPerson.PREV_NAME + '|' + arrPerson.PREV_OTCH + '|' + arrPerson.PREV_BIRTHDAY + '|' + arrPerson.SYS_IS_REPLICATION + '|' + arrPerson.DEATH_NUM + '|' + arrPerson.DEATH_DATE + '|' + arrPerson.REGOPER + '|' + arrPerson.RPADRD + '|' + arrPerson.RPADRM + '|' + arrPerson.RPADRY + '|' + arrPerson.SPOTM + '|' + arrPerson.D_VVOD + '|' ;
		

		/*** 3 ***/
		
		var 
			arrAdrBirthplace =  ParseBirthAddresses(person);
		//strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.REGION  +'|' + arrAdrBirthplace.RAION  +'|' + '|'+ arrAdrBirthplace.CITY_TYPE +'|' + arrAdrBirthplace.CITY + '|' + '|' + '||||0|' + "\r\n";
		strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.ALL  +'|' + ''  +'|' + '|'+ '' +'|' + '' + '|' + '|' + '||||0|' + "\r\n";
		//нынешнее метсо жительства

		var 
			arrAdr		= ParseAddresses(adrAll,adrHouseFlat),
			arrAdrPrev	= ParseAddresses(adrAllPrev,adrHouseFlatPrev);

		strAddress += listID + '2'+ '|'+subdivision + '00' + listID + '|' + '1' + '|' +'РОССИЯ'+ '|' + arrAdr.ADR_OBL + '|' + arrAdr.ADR_RAION + '|' + '|' + arrAdr.ADR_CITY_TYPE + '|' + arrAdr.ADR_CITY + '|' + arrAdr.ADR_STREET +' ' + arrAdr.ADR_STREET_PR + '.|' + arrAdr.ADR_HOUSE + '|' + arrAdr.ADR_HOUSE_LITERA + '|' + arrAdr.ADR_FLAT + '|' + '|' + "\r\n";
		//strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +'РОССИЯ'+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +''+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		
		//вставляем значение в массив форм
		arrOutputDataForms.push(strForm);

		//вставляем значение в массив адресов
		arrOutputDataAddress.push(strAddress);			
		WriteStrIntoFileData();
		return true;		


}

function ParseRussianPassport(obj)
{	
	var
		subdivision			= GetTerritoryCodeToDB(obj[parserCfg.RUSSIAN_PASSPORT.subdivision]),
		listID				= obj[parserCfg.RUSSIAN_PASSPORT.listID],
		employer			= obj[parserCfg.RUSSIAN_PASSPORT.employer],
		person				= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.person),	
		prevPerson			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.prevPerson),	
		documents			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.documents),
		prevAddress			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.prevAddress),
		applPlace			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.applPlace),
		/*
		subdivision			= GetTerritoryCodeToDB(obj.number),
		listID				= obj.uid,
		employer			= obj.employee,
		person				= obj['ns2:newApplicant'].person,		
		prevPerson			= _Get(obj['ns2:applicant'],'person'),
		documents			= obj['ns2:newApplicant'].document,
		prevAddress			= obj['ns17:previousRegistrationAddress'],
		applPlace			= obj['ns2:applicationPlace'],
		*/
		livAddress;

	if (_GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.livAddress) === '') {
		livAddress			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.alter_livAddress);	
	}
	else {
		livAddress			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.livAddress);	
	}


	if (livAddress !== '') {
		var 
			adrAll				= livAddress.addressObject.value;
			adrHouseFlat		= livAddress.housing;
	}
	else {
		var 
			adrAll				= '';
			adrHouseFlat		= '';
	} 
	
	var
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '',
		strForm				= '',
		strAddress			= '';		

		if (prevAddress !== '')
		{
			livAddressPrev		= prevAddress.address.russianAddress;
			adrAllPrev			= livAddressPrev.addressObject.value;
			adrHouseFlatPrev	= livAddressPrev.housing;	
		}				
		
		var 
			arrPerson = {};

		arrPerson.LASTNAME		= person.lastName;
		arrPerson.FIRSTNAME		= person.firstName;
		arrPerson.MIDDLENAME	= _Get(person,'middleName');
		arrPerson.BIRTH			= person.birthDate;
		arrPerson.GR			= person.citizenship.value;

		if (person.gender.element === 'M') {
			arrPerson.SEX = 1;	
		}
		else {
			arrPerson.SEX = 2;
		}
		
		// прибытие
		arrPerson.REGISTRATION_TYPE = '1';	

		/*
		<ns2:applicationPlace>
			<type>applicationPlace</type>
			<element>8003</element>
			<value>По месту фактического обращения</value>
		</ns2:applicationPlace>
		*/
		// по месту обращения
		if (_Get(applPlace,'element') === '8003') {
			arrPerson.REGISTERED		= 'BR';		
		}
		else {
			arrPerson.REGISTERED		= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.registered);					
		}
		
		arrPerson.ORGAN				= subdivision;

		arrPerson.DOC_TYPE		= GetDocTypeToDB(_Get(documents,'type').element);				
		arrPerson.DOC_SERIES	= _Get(documents,'series');		
		arrPerson.DOC_NUM		= _Get(documents,'number');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan.element')+'z'+_Get(documents,'authorityOrgan.value');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan').value;	
		if (arrPerson.DOC_ORGAN === undefined) {
			arrPerson.DOC_ORGAN		= _Get(documents,'authority');		
		}	
		arrPerson.DOC_DATE		= _Get(documents,'issued');		
		arrPerson.CHANGES		= '';		
		arrPerson.OTHER			= '';		
		//arrPerson.REASON		= GetPassReasonToDB(_Get(obj,'ns2:applicationReason').element);
		arrPerson.REASON		= GetPassReasonToDB(_GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.reason));
		//

		if(arrPerson.REASON === '') {
			arrPerson.REASON	= '4';
			arrPerson.OTHER		= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.other);	
		}	
		
		//ParseFormReasons(_Get(obj,'ns2:applicationReason').value,_Get(obj,'ns2:applicationReason').element);
		arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.creation_date);	
		arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.check_date);		
		
		arrPerson.SYS_IS_REPLICATION	= '';
		arrPerson.DEATH_NUM				= '';
		arrPerson.DEATH_DATE			= '';
		arrPerson.REGOPER				= _Get(employer,'name');

		if (_GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.livAddress) !== '') { 
			if (_GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.rpadr) !== '') {
				arrPerson.RPADR					= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.rpadr);
				arrPerson.RPADRD				= arrPerson.RPADR.substr(8,2);
				arrPerson.RPADRM				= arrPerson.RPADR.substr(5,2);
				arrPerson.RPADRY				= arrPerson.RPADR.substr(0,4);
			}
			else if (_GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.alt_rpadr) !== '') {
				arrPerson.RPADR					= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.alt_rpadr);
				arrPerson.RPADRD				= arrPerson.RPADR.substr(8,2);
				arrPerson.RPADRM				= arrPerson.RPADR.substr(5,2);
				arrPerson.RPADRY				= arrPerson.RPADR.substr(0,4);
			}
		}
		else {
			arrPerson.RPADRD				= '';
			arrPerson.RPADRM				= '';
			arrPerson.RPADRY				= '';	
		}
		
		arrPerson.SPOTM					= '';
		arrPerson.D_VVOD				= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT.d_vvod);

		if (arrPerson.REASON === '3' ) {			

			arrPerson.PREV_FAM				= prevPerson.lastName;
			arrPerson.PREV_NAME				= prevPerson.firstName;
			arrPerson.PREV_OTCH				= _Get(prevPerson,'middleName');
			arrPerson.PREV_BIRTHDAY			= prevPerson.birthDate;	

			GetF7FormForParsing(obj);

		}
		else {
			arrPerson.PREV_FAM				= '';
			arrPerson.PREV_NAME				= '';
			arrPerson.PREV_OTCH				= '';
			arrPerson.PREV_BIRTHDAY			= '';
		}
		
		strForm += subdivision + '00' + listID + '|' + arrPerson.REGISTRATION_TYPE + '|' + arrPerson.LASTNAME + '|' + arrPerson.FIRSTNAME + '|' + arrPerson.MIDDLENAME + '|' + arrPerson.BIRTH + '|' + arrPerson.GR + '|' + '|' + arrPerson.SEX + '|' + '|' + arrPerson.REGISTERED + '|' + arrPerson.ORGAN + '|' ;
		strForm += arrPerson.DOC_TYPE + '|' + arrPerson.DOC_SERIES + '|' + arrPerson.DOC_NUM + '|' + arrPerson.DOC_ORGAN + '|' + arrPerson.DOC_DATE + '|' + arrPerson.CHANGES +'|'+ arrPerson.OTHER +'|' + arrPerson.REASON + '|' + arrPerson.CREATION_DATE + '|';
		strForm += arrPerson.CHECK_DATE + '|' + arrPerson.PREV_FAM + '|' + arrPerson.PREV_NAME + '|' + arrPerson.PREV_OTCH + '|' + arrPerson.PREV_BIRTHDAY + '|' + arrPerson.SYS_IS_REPLICATION + '|' + arrPerson.DEATH_NUM + '|' + arrPerson.DEATH_DATE + '|' + arrPerson.REGOPER + '|' + arrPerson.RPADRD + '|' + arrPerson.RPADRM + '|' + arrPerson.RPADRY + '|' + arrPerson.SPOTM + '|' + arrPerson.D_VVOD + '|' ;
		

		/*** 3 ***/
		
		var 
			arrAdrBirthplace =  ParseBirthAddresses(person);
		//strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.REGION  +'|' + arrAdrBirthplace.RAION  +'|' + '|'+ arrAdrBirthplace.CITY_TYPE +'|' + arrAdrBirthplace.CITY + '|' + '|' + '||||0|' + "\r\n";
		strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.ALL  +'|' + ''  +'|' + '|'+ '' +'|' + '' + '|' + '|' + '||||0|' + "\r\n";
		//нынешнее метсо жительства

		var
			arrAdr		= ParseAddresses(adrAll,adrHouseFlat),
			arrAdrPrev	= ParseAddresses(adrAllPrev,adrHouseFlatPrev);

		strAddress += listID + '2'+ '|'+subdivision + '00' + listID + '|' + '1' + '|' +'РОССИЯ'+ '|' + arrAdr.ADR_OBL + '|' + arrAdr.ADR_RAION + '|' + '|' + arrAdr.ADR_CITY_TYPE + '|' + arrAdr.ADR_CITY + '|' + arrAdr.ADR_STREET +' ' + arrAdr.ADR_STREET_PR + '.|' + arrAdr.ADR_HOUSE + '|' + arrAdr.ADR_HOUSE_LITERA + '|' + arrAdr.ADR_FLAT + '|' + '|' + "\r\n";
		//strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +'РОССИЯ'+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +''+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		
		//вставляем значение в массив форм
		arrOutputDataForms.push(strForm);

		//вставляем значение в массив адресов
		arrOutputDataAddress.push(strAddress);			
		WriteStrIntoFileData();
		return true;		

}

function GetF7FormForParsing(obj)
{	
	var
		/*
		subdivision			= GetTerritoryCodeToDB(obj.number),
		listID				= '9' + obj.uid,
		employer			= obj.employee,
		person				= obj['ns2:applicant']['person'],		
		documents			= obj['ns2:applicant'].document,
		prevAddress			= obj['ns17:previousRegistrationAddress'],
		applPlace			= obj['ns2:applicationPlace'],
		livAddress;
		*/
 
		subdivision			= GetTerritoryCodeToDB(obj[parserCfg.RUSSIAN_PASSPORT_F7.subdivision]),
		listID				= '9' + obj[parserCfg.RUSSIAN_PASSPORT_F7.listID],
		employer			= obj[parserCfg.RUSSIAN_PASSPORT_F7.employer],
		person				= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.person),	
		prevPerson			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.prevPerson),
		documents			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.documents),
		prevAddress			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.prevAddress),
		applPlace			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.applPlace),
		livAddress;

	if (_GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.livAddress) === '') {
		livAddress			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.alter_livAddress);	
	}
	else {
		livAddress			= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.livAddress);	
	}


	if (livAddress !== '') {
		var 
			adrAll				= livAddress.addressObject.value;
			adrHouseFlat		= livAddress.housing;
	}
	else {
		var 
			adrAll				= '';
			adrHouseFlat		= '';
	} 


	var 
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '',
		strForm				= '',
		strAddress			= '';		

		if (prevAddress !== '')
		{
			livAddressPrev		= prevAddress.address.russianAddress;
			adrAllPrev			= livAddressPrev.addressObject.value;
			adrHouseFlatPrev	= livAddressPrev.housing;	
		}				
		

		

		var 
			arrPerson = {};

		arrPerson.LASTNAME		= person.lastName;
		arrPerson.FIRSTNAME		= person.firstName;
		arrPerson.MIDDLENAME	= _Get(person,'middleName');
		arrPerson.BIRTH			= person.birthDate;
		arrPerson.GR			= person.citizenship.value;

		if (person.gender.element === 'M') {
			arrPerson.SEX = 1;	
		}
		else {
			arrPerson.SEX = 2;
		}
		
		arrPerson.REGISTRATION_TYPE = '2';
		arrPerson.REGISTERED		= '';	
		arrPerson.ORGAN				= subdivision;

		arrPerson.DOC_TYPE		= GetDocTypeToDB(_Get(documents,'type').element);				
		arrPerson.DOC_SERIES	= _Get(documents,'series');		
		arrPerson.DOC_NUM		= _Get(documents,'number');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan.element')+'z'+_Get(documents,'authorityOrgan.value');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan').value;	
		if (arrPerson.DOC_ORGAN === undefined) {
			arrPerson.DOC_ORGAN		= _Get(documents,'authority');		
		}	
		arrPerson.DOC_DATE		= _Get(documents,'issued');		
		arrPerson.CHANGES		= '';		
		arrPerson.OTHER			= '';		
		arrPerson.REASON		= '27';

		arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.creation_date);	
		arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.check_date);		

		/*
		arrPerson.PREV_FAM				= '';
		arrPerson.PREV_NAME				= '';
		arrPerson.PREV_OTCH				= '';
		arrPerson.PREV_BIRTHDAY			= '';
		*/

		arrPerson.PREV_FAM				= prevPerson.lastName;
		arrPerson.PREV_NAME				= prevPerson.firstName;
		arrPerson.PREV_OTCH				= _Get(prevPerson,'middleName');
		arrPerson.PREV_BIRTHDAY			= prevPerson.birthDate;	
		
		arrPerson.SYS_IS_REPLICATION	= '';
		arrPerson.DEATH_NUM				= '';
		arrPerson.DEATH_DATE			= '';
		arrPerson.REGOPER				= _Get(employer,'name');

		if (_GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.livAddress) !== '') {
			arrPerson.RPADR					= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.rpadr);
			arrPerson.RPADRD				= arrPerson.RPADR.substr(8,2);
			arrPerson.RPADRM				= arrPerson.RPADR.substr(5,2);
			arrPerson.RPADRY				= arrPerson.RPADR.substr(0,4);
		}
		else {
			arrPerson.RPADRD				= '';
			arrPerson.RPADRM				= '';
			arrPerson.RPADRY				= '';	
		}
		
		arrPerson.SPOTM					= '';
		arrPerson.D_VVOD				= _GetRealPath(obj,parserCfg.RUSSIAN_PASSPORT_F7.d_vvod);

		strForm += subdivision + '00' + listID + '|' + arrPerson.REGISTRATION_TYPE + '|' + arrPerson.LASTNAME + '|' + arrPerson.FIRSTNAME + '|' + arrPerson.MIDDLENAME + '|' + arrPerson.BIRTH + '|' + arrPerson.GR + '|' + '|' + arrPerson.SEX + '|' + '|' + arrPerson.REGISTERED + '|' + arrPerson.ORGAN + '|' ;
		strForm += arrPerson.DOC_TYPE + '|' + arrPerson.DOC_SERIES + '|' + arrPerson.DOC_NUM + '|' + arrPerson.DOC_ORGAN + '|' + arrPerson.DOC_DATE + '|' + arrPerson.CHANGES +'|'+ arrPerson.OTHER +'|' + arrPerson.REASON + '|' + arrPerson.CREATION_DATE + '|';
		strForm += arrPerson.CHECK_DATE + '|' + arrPerson.PREV_FAM + '|' + arrPerson.PREV_NAME + '|' + arrPerson.PREV_OTCH + '|' + arrPerson.PREV_BIRTHDAY + '|' + arrPerson.SYS_IS_REPLICATION + '|' + arrPerson.DEATH_NUM + '|' + arrPerson.DEATH_DATE + '|' + arrPerson.REGOPER + '|' + arrPerson.RPADRD + '|' + arrPerson.RPADRM + '|' + arrPerson.RPADRY + '|' + arrPerson.SPOTM + '|' + arrPerson.D_VVOD + '|' ;
		
		

		/*** 3 ***/
		var 
			arrAdrBirthplace =  ParseBirthAddresses(person);
		//strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.REGION  +'|' + arrAdrBirthplace.RAION  +'|' + '|'+ arrAdrBirthplace.CITY_TYPE +'|' + arrAdrBirthplace.CITY + '|' + '|' + '||||0|' + "\r\n";
		strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.ALL  +'|' + ''  +'|' + '|'+ '' +'|' + '' + '|' + '|' + '||||0|' + "\r\n";
		//нынешнее метсо жительства

		var
			arrAdr		= ParseAddresses(adrAll,adrHouseFlat),
			arrAdrPrev	= ParseAddresses(adrAllPrev,adrHouseFlatPrev);

		strAddress += listID + '2'+ '|'+subdivision + '00' + listID + '|' + '1' + '|' +'РОССИЯ'+ '|' + arrAdr.ADR_OBL + '|' + arrAdr.ADR_RAION + '|' + '|' + arrAdr.ADR_CITY_TYPE + '|' + arrAdr.ADR_CITY + '|' + arrAdr.ADR_STREET +' ' + arrAdr.ADR_STREET_PR + '.|' + arrAdr.ADR_HOUSE + '|' + arrAdr.ADR_HOUSE_LITERA + '|' + arrAdr.ADR_FLAT + '|' + '|' + "\r\n";
		//strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +'РОССИЯ'+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +''+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		//вставляем значение в массив форм
		arrOutputDataForms.push(strForm);

		//вставляем значение в массив адресов
		arrOutputDataAddress.push(strAddress);	

		return true;		

}

function ParseUnregByStaying(obj)
{

	var
		/*
		subdivision			= GetTerritoryCodeToDB(obj.number),
		listID				= obj.uid,
		employer			= obj.employee,
		person				= obj['ns22:declarant'].person,		
		documents			= obj['ns22:declarant'].document,
		prevAddress			= obj['ns22:previousRegistrationAddress'],

		livAddress			= obj['ns25:stayingRegistrationData'].address.russianAddress,
		adrAll				= livAddress.addressObject.value,
		adrHouseFlat		= livAddress.housing,		
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '';
		*/
		
		subdivision			= GetTerritoryCodeToDB(obj[parserCfg.UNREG_BY_STAYING.subdivision]),
		listID				= obj[parserCfg.UNREG_BY_STAYING.listID],
		employer			= obj[parserCfg.UNREG_BY_STAYING.employer],
		person				= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.person),		
		documents			= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.documents),
		prevAddress			= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.prevAddress),

		livAddress			= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.livAddress),		
		livAddressPrev		= '',
		adrAllPrev			= '',
		adrHouseFlatPrev	= '';

		if (_GetRealPath(obj,parserCfg.UNREG_BY_STAYING.short_form) !== '') {
			return false;
		}

		if (livAddress !== '') {
			var 
				adrAll				= livAddress.addressObject.value;
				adrHouseFlat		= livAddress.housing;
		}
		else {
			var 
				adrAll				= '';
				adrHouseFlat		= '';
		} 
		

		if (prevAddress !== '') {
			/* old
			livAddressPrev		= prevAddress.address.russianAddress;
			adrAllPrev			= livAddressPrev.addressObject.value;
			adrHouseFlatPrev	= livAddressPrev.housing;	
			*/

			livAddressPrev		= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.livAddressPrev);
			if(livAddressPrev !== '') {
				adrAllPrev			= livAddressPrev.addressObject.value;
				adrHouseFlatPrev	= livAddressPrev.housing;		
			}
			
		}		
	
		var 
			arrPerson			= {},
			strForm				= '',
			strAddress			= '';		

		arrPerson.LASTNAME		= person.lastName;
		arrPerson.FIRSTNAME		= person.firstName;
		arrPerson.MIDDLENAME	= _Get(person,'middleName');
		arrPerson.BIRTH			= person.birthDate;
		arrPerson.GR			= person.citizenship.value;

		if (person.gender.element === 'M') {
			arrPerson.SEX = 1;	
		}
		else {
			arrPerson.SEX = 2;
		}
		
		arrPerson.REGISTRATION_TYPE = '2';
		//убираем регистрацию, чтобы парсер определил её как форму убытия
		//arrPerson.REGISTERED		= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.registered);	
		arrPerson.REGISTERED		= '';
		arrPerson.ORGAN				= subdivision;

		arrPerson.DOC_TYPE		= GetDocTypeToDB(_Get(documents,'type').element);		
		arrPerson.DOC_SERIES	= _Get(documents,'series');		
		arrPerson.DOC_NUM		= _Get(documents,'number');		
		arrPerson.DOC_ORGAN		= _Get(documents,'authorityOrgan').value;
		if (arrPerson.DOC_ORGAN === undefined) {
			arrPerson.DOC_ORGAN		= _Get(documents,'authority');		
		}		
		arrPerson.DOC_DATE		= _Get(documents,'issued');		
		arrPerson.CHANGES		= '';		

		arrPerson.OTHER			= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.other);				
		arrPerson.OTHER			= arrPerson.OTHER.substr(0,30);		
		arrPerson.REASON		= GetPassReasonToDB(_GetRealPath(obj,parserCfg.UNREG_BY_STAYING.reason));		
		
		if (arrPerson.REASON === '') {
			arrPerson.REASON = '43';
		}
		else if (arrPerson.REASON === '41') {
			arrPerson.REASON = '41';
		}
		
		arrPerson.CREATION_DATE = _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.creation_date);	
		arrPerson.CHECK_DATE	= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.check_date);

		arrPerson.PREV_FAM				= '';
		arrPerson.PREV_NAME				= '';
		arrPerson.PREV_OTCH				= '';
		arrPerson.PREV_BIRTHDAY			= '';
		arrPerson.SYS_IS_REPLICATION	= '';
		arrPerson.DEATH_NUM				= '';
		arrPerson.DEATH_DATE			= '';
		arrPerson.REGOPER				= _Get(employer,'name');

		if (_GetRealPath(obj,parserCfg.UNREG_BY_STAYING.livAddressAll) !== '') {
			arrPerson.RPADR					= _Get(_GetRealPath(obj,parserCfg.UNREG_BY_STAYING.livAddressAll),'dateFrom');			
			arrPerson.RPADRD				= arrPerson.RPADR.substr(8,2);
			arrPerson.RPADRM				= arrPerson.RPADR.substr(5,2);
			arrPerson.RPADRY				= arrPerson.RPADR.substr(0,4);
		}
		else {
			arrPerson.RPADRD				= '';
			arrPerson.RPADRM				= '';
			arrPerson.RPADRY				= '';	
		}

		arrPerson.SPOTM					= '';
		arrPerson.D_VVOD				= _GetRealPath(obj,parserCfg.UNREG_BY_STAYING.d_vvod);
		
		/*strForm += subdivision + '00' + listID + '|' + arrPerson.REGISTRATION_TYPE + '|' + arrPerson.LASTNAME + '|' + arrPerson.FIRSTNAME + '|' + arrPerson.MIDDLENAME + '|' + arrPerson.BIRTH + '|' + arrPerson.GR + '|' + '|' + arrPerson.SEX + '|' + '|' + arrPerson.REGISTERED + '|' + arrPerson.ORGAN + '|' ;
		strForm += arrPerson.DOC_TYPE + '|' + arrPerson.DOC_SERIES + '|' + arrPerson.DOC_NUM + '|' + arrPerson.DOC_ORGAN + '|' + arrPerson.DOC_DATE + '|' + arrPerson.CHANGES +'|'+ arrPerson.OTHER +'|' + arrPerson.REASON + '|' + arrPerson.CREATION_DATE + '|';
		strForm += arrPerson.CHECK_DATE + '|' + arrPerson.PREV_FAM + '|' + arrPerson.PREV_NAME + '|' + arrPerson.PREV_OTCH + '|' + arrPerson.PREV_BIRTHDAY + '|' + arrPerson.SYS_IS_REPLICATION + '|' + arrPerson.DEATH_NUM + '|' + arrPerson.DEATH_DATE + '|' + arrPerson.REGOPER + '|' + arrPerson.RPADRD + '|' + arrPerson.RPADRM + '|' + arrPerson.RPADRY + '|' + arrPerson.SPOTM + '|' + arrPerson.D_VVOD + '|' ;
		*/
		strForm += subdivision + '00' + listID + '|' + arrPerson.REGISTRATION_TYPE + '|' + arrPerson.LASTNAME + '|' + arrPerson.FIRSTNAME + '|' + arrPerson.MIDDLENAME + '|' + arrPerson.BIRTH + '|' + arrPerson.GR + '|' + '|' + arrPerson.SEX + '|' + '|' + arrPerson.REGISTERED + '|' + arrPerson.ORGAN + '|' ;
		strForm += arrPerson.DOC_TYPE + '|' + arrPerson.DOC_SERIES + '|' + arrPerson.DOC_NUM + '|' + arrPerson.DOC_ORGAN + '|' + arrPerson.DOC_DATE + '|' + arrPerson.CHANGES +'|'+ '' +'|' + arrPerson.REASON + '|' + arrPerson.CREATION_DATE + '|';
		strForm += arrPerson.CHECK_DATE + '|' + arrPerson.PREV_FAM + '|' + arrPerson.PREV_NAME + '|' + arrPerson.PREV_OTCH + '|' + arrPerson.PREV_BIRTHDAY + '|' + arrPerson.SYS_IS_REPLICATION + '|' + arrPerson.DEATH_NUM + '|' + arrPerson.DEATH_DATE + '|' + arrPerson.REGOPER + '|' + arrPerson.RPADRD + '|' + arrPerson.RPADRM + '|' + arrPerson.RPADRY + '|' + arrPerson.SPOTM + '|' + arrPerson.D_VVOD + '|' ;

		
		/*** 3 ***/
		
		var 
			arrAdrBirthplace =  ParseBirthAddresses(person);
		//strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.REGION  +'|' + arrAdrBirthplace.RAION  +'|' + '|'+ arrAdrBirthplace.CITY_TYPE +'|' + arrAdrBirthplace.CITY + '|' + '|' + '||||0|' + "\r\n";
		strAddress += listID + '1'+ '|'+subdivision + '00' + listID + '|' + '3' + '|' + arrAdrBirthplace.COUNTRY + '|' + arrAdrBirthplace.ALL  +'|' + ''  +'|' + '|'+ '' +'|' + '' + '|' + '|' + '||||0|' + "\r\n";
		//нынешнее метсо жительства

		var 
			arrAdr		= ParseAddresses(adrAll,adrHouseFlat),
			arrAdrPrev	= ParseAddresses(adrAllPrev,adrHouseFlatPrev);

		strAddress += listID + '2'+ '|'+subdivision + '00' + listID + '|' + '1' + '|' +'РОССИЯ'+ '|' + arrAdr.ADR_OBL + '|' + arrAdr.ADR_RAION + '|' + '|' + arrAdr.ADR_CITY_TYPE + '|' + arrAdr.ADR_CITY + '|' + arrAdr.ADR_STREET +' ' + arrAdr.ADR_STREET_PR + '.|' + arrAdr.ADR_HOUSE + '|' + arrAdr.ADR_HOUSE_LITERA + '|' + arrAdr.ADR_FLAT + '|' + '|' + "\r\n";
		//strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +'РОССИЯ'+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		strAddress += listID + '3'+ '|'+subdivision + '00' + listID + '|' + '2' + '|' +''+ '|' + arrAdrPrev.ADR_OBL + '|' + arrAdrPrev.ADR_RAION + '|' + '|' + arrAdrPrev.ADR_CITY_TYPE + '|' + arrAdrPrev.ADR_CITY + '|' + arrAdrPrev.ADR_STREET +' ' + arrAdrPrev.ADR_STREET_PR + '.|' + arrAdrPrev.ADR_HOUSE + '|' + arrAdrPrev.ADR_HOUSE_LITERA + '|' + arrAdrPrev.ADR_FLAT + '|' + '|';
		
		//вставляем значение в массив форм
		arrOutputDataForms.push(strForm);
		
		//вставляем значение в массив адресов
		arrOutputDataAddress.push(strAddress);	

		WriteStrIntoFileData();
		return true;		

}


//записываем обработанные данные в файл
function WriteFileData()
{	
	
	var fs	= require('fs');	

	var 
		strOutputForms		= '',
		strOutputAddress	= '',
		strFileOutputCoding	= 'UTF-8';				

	//формируем строку форм
	for(var i in arrOutputDataForms) {
		if (arrOutputDataForms[i] !== '') {
			strOutputForms += arrOutputDataForms[i] + "\r\n";	
		}
	}	

	////формируем строку адресов
	for(i in arrOutputDataAddress) {
		if (arrOutputDataAddress[i] !== '') {
			strOutputAddress += arrOutputDataAddress[i] + "\r\n" ;	
		}		
	}	


	var 
		fileHandler = fs.openSync('./data/output/forms.dat', 'w', 0666),
		numberBytes = fs.writeSync(fileHandler,strOutputForms,null,strFileOutputCoding);		 

	console.log('forms.dat added :' + numberBytes);
	fs.closeSync(fileHandler);	

	fileHandler = fs.openSync('./data/output/address.dat', 'w', 0666);
	numberBytes = fs.writeSync(fileHandler,strOutputAddress,null,strFileOutputCoding);
	console.log('address.dat added :' + numberBytes);
	fs.closeSync(fileHandler);
	

	return true;
	
}

//записываем обработанные данные в файл
function WriteStrIntoFileData()
{	
	
	var fs	= require('fs');	

	var 
		strOutputForms		= '',
		strOutputAddress	= '',		
		strFileOutputCoding	= 'UTF-8';

	//формируем строку форм
	for(var i in arrOutputDataForms) {
		if (arrOutputDataForms[i] !== '') {
			strOutputForms += arrOutputDataForms[i] + "\r\n";	
		}
	}	

	////формируем строку адресов
	for(i in arrOutputDataAddress) {
		if (arrOutputDataAddress[i] !== '') {
			strOutputAddress += arrOutputDataAddress[i] + "\r\n" ;	
		}		
	}	


	var 
		fileHandler = fs.openSync('./data/output/forms.dat', 'a', 0666),
		numberBytes = fs.writeSync(fileHandler,ConvertToWin(strOutputForms),null,strFileOutputCoding);		 
	
	fs.closeSync(fileHandler);	

	fileHandler = fs.openSync('./data/output/address.dat', 'a', 0666);
	numberBytes = fs.writeSync(fileHandler,ConvertToWin(strOutputAddress),null,strFileOutputCoding);	
	fs.closeSync(fileHandler);

	arrOutputDataForms		= [];
	arrOutputDataAddress	= [];	

	return true;
	
}


function WriteErrorData(filename,error)
{	
	var fs	= require('fs');	

	var 
		strOutput		= filename + "\r\n" + error + "\r\n",		
		strFileOutputCoding	= 'UTF-8';
	
	var fileHandler = fs.openSync('./data/output/errors.txt', 'a', 0666);
		fs.writeSync(fileHandler,strOutput,null,strFileOutputCoding);
		fs.closeSync(fileHandler);
	return true;	
	
}


function SaveDictionaryData(strData)
{	
	var fs	= require('fs');	

	var 
		strOutput		= '',		
		strFileOutputCoding	= 'UTF-8';
		strOutput = strData + "\r\n" ;
	

	
	var fileHandler = fs.openSync('./data/output/dict.txt', 'a', 0666);
		fs.writeSync(fileHandler,strOutput,null,strFileOutputCoding);
		fs.closeSync(fileHandler);
	return true;	
	
}

