
exports.GetTerritories = function()
{

  var arrData =  
  [ 
    { element   : ['740002'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В АГАПОВСКОМ Р-НЕ',
      DB        : '1' },  

    { element   : ['740003'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В АРГАЯШСКОМ Р-НЕ',
      DB        : '2' },  

    { element   : ['740007'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В БРЕДИНСКОМ Р-НЕ',
      DB        : '3' },   

    { element   : ['740008'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ВАРНЕНСКОМ Р-НЕ',
      DB        : '4' },

    { element   : ['740009'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ВЕРХНЕУРАЛЬСКОМ Р-НЕ',
      DB        : '5' },

    { element   : ['740004','740005','740006'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В АШИНСКОМ Р-НЕ',
      DB        : '6' },

    { element   : ['740010'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. ЕМАНЖЕЛИНСКЕ',
      DB        : '7' },   

    { element   : ['740014'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г.ЗЛАТОУСТЕ',
      DB        : '9' },

    { element   : ['740012'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ЕТКУЛЬСКОМ Р-НЕ',
      DB        : '10' },

    { element   : ['740015'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. КАРАБАШЕ',
      DB        : '11' },

    { element   : ['740016'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КАРТАЛИНСКОМ Р-НЕ',
      DB        : '12' },

    { element   : ['740017'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КАСЛИНСКОМ Р-НЕ',
      DB        : '13' },  

    { element   : ['740018','740019'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КАТАВ-ИВАНОВСКОМ Р-НЕ',
      DB        : '14' },  

	  { element   : ['740020'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КИЗИЛЬСКОМ Р-НЕ',
      DB        : '15' },

    { element   : ['740021'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. КОПЕЙСКЕ',
      DB        : '16' },   

    { element   : ['740022'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г.КОРКИНО',
      DB        : '17' },   

    { element   : ['740023'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КРАСНОАРМЕЙСКОМ Р-НЕ',
      DB        : '18' },   

    { element   : ['740024'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КУНАШАКСКОМ Р-НЕ',
      DB        : '19' },   

    { element   : ['740025'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КУСИНСКОМ Р-НЕ',
      DB        : '20' },

    { element   : ['740026'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. КЫШТЫМЕ',
      DB        : '21' },  

    { element   : ['740029'],
      value     : 'ОТДЕЛЕНИЕ №3 УФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ЛЕНИНСКОМ РАЙОНЕ Г.МАГНИТОГОРСКА',
      DB        : '22' },

    { element   : ['740027'],
      value     : 'ОТДЕЛЕНИЕ №1 УФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ОРДЖОНИКИДЗЕВСКОМ РАЙОНЕ Г.МАГНИТОГОРСКА',
      DB        : '23' },

    { element   : ['740028'],
      value     : 'ОТДЕЛЕНИЕ №2 УФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ПРАВОБЕРЕЖНОМ РАЙОНЕ Г.МАГНИТОГОРСКА',
      DB        : '24' },

    { element   : ['740030','740031'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. МИАССЕ',
      DB        : '25' },

    { element   : ['740032'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В НАГАЙБАКСКОМ Р-НЕ',
      DB        : '26' },

    { element   : ['740033'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В НЯЗЕПЕТРОВСКОМ Р-НЕ',
      DB        : '27' },

    { element   : ['740034'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ОКТЯБРЬСКОМ Р-НЕ',
      DB        : '28' },   

    { element   : ['740035'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г.ПЛАСТЕ',
      DB        : '29' },        

    { element   : ['740036','740037'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В САТКИНСКОМ Р-НЕ',
      DB        : '30' },        

    { element   : ['740038'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В СОСНОВСКОМ Р-НЕ',
      DB        : '31' },        
   
    { element   : ['740039'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. ТРОИЦКЕ И ТРОИЦКОМ Р-НЕ',
      DB        : '32' },

    { element   : ['740040'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В УВЕЛЬСКОМ Р-НЕ',
      DB        : '34' }, 

    { element   : ['740041'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В УЙСКОМ Р-НЕ',
      DB        : '35' }, 

    { element   : ['740043'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. УСТЬ-КАТАВЕ',
      DB        : '36' },

    { element   : ['740042'],
      value     : ' ОТДЕЛЕНИЕ УФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. ВЕРХНИЙ УФАЛЕЙ',
      DB        : '37' },

    { element   : ['740044'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ЧЕБАРКУЛЬСКОМ Р-НЕ',
      DB        : '38' }, 

    { element   : ['740045'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ЧЕСМЕНСКОМ Р-НЕ',
      DB        : '39' },

    { element   : ['740046'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г. ЮЖНОУРАЛЬСКЕ',
      DB        : '40' },

    { element   : ['740054'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ЛЕНИНСКОМ Р-НЕ Г.ЧЕЛЯБИНСКА',
      DB        : '41' }, 

    { element   : ['740055'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В МЕТАЛЛУРГИЧЕСКОМ Р-НЕ Г.ЧЕЛЯБИНСКА',
      DB        : '42' },    

    { element   : ['740056'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В СОВЕТСКОМ Р-НЕ',
      DB        : '43' },

    { element   : ['740057'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ТРАКТОРОЗАВОДСКОМ Р-НЕ Г.ЧЕЛЯБИНСКА',
      DB        : '44' },

    { element   : ['740058'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В ЦЕНТРАЛЬНОМ Р-НЕ Г.ЧЕЛЯБИНСКА',
      DB        : '45' },    

    { element   : ['740052'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КАЛИНИНСКОМ Р-НЕ Г. ЧЕЛЯБИНСКА',
      DB        : '46' },  

    { element   : ['740053'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В КУРЧАТОВСКОМ Р-НЕ',
      DB        : '47' },

    { element   : ['740050'],
      value     : 'ТП УФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В П.ТРЕХГОРНЫЙ-1',
      DB        : '55' },                    

    { element   : ['740049'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г.ТРЁХГОРНЫЙ',
      DB        : '56' },                    
    
    { element   : ['740047'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г.СНЕЖИНСКЕ',
      DB        : '59' },       

    { element   : ['740048'],
      value     : 'ОУФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ. В Г.ОЗЕРСКЕ',
      DB        : '60' },
	  
	{ element   : ['740001'],
      value     : 'УПРАВЛЕНИЕ ФМС РОССИИ ПО ЧЕЛЯБИНСКОЙ ОБЛ.',
      DB        : '91' }
  ];
  return arrData;
};

exports.GetDocTypes = function()
{
  var arrData = [    
    { element   : '103008',
      value     : 'Паспорт гражданина Российской Федерации',
      DB        : '1' },  

    { element   : '102974',
      value     : 'Свидетельство о рождении',
      DB        : '3' },  

    { element   : '103014',
      value     : 'Иностранное свидетельство о рождении',
      DB        : '3' },  

    { element   : '136184',
      value     : 'Свидетельство о рождении, выданное за пределами РФ',
      DB        : '3' }, 

    { element   : '103007',
      value     : 'Загранпаспорт гражданина Российской Федерации',
      DB        : '5' },

    { element   : '139370',
      value     : 'Загранпаспорт, выданный за пределами Российской Федерации',
      DB        : '5' }
  ];
  return arrData;
  /*

  Паспорт гражданина Российской Федерации 103008
  Свидетельство о рождении 102974
  Иностранное свидетельство о рождении 103014
  Свидетельство о рождении, выданное за пределами РФ 136184
  Загранпаспорт гражданина Российской Федерации 103007
  Загранпаспорт, выданный за пределами Российской Федерации 139370



  1   ПАСПОРТ                                                                                                                          
  2   УДОСТОВЕРЕНИЕ                                                                                                                    
  3   СВИДЕТЕЛЬСТВО О РОЖДЕНИИ                                                                                                         
  4   ВИД НА ЖИТЕЛЬСТВО                                                                                                                
  5   ЗАГРАНИЧНЫЙ ПАСПОРТ                                                                                                              
  6   СПРАВКА Ф 9                                                                                                                      
  7   СПРАВКА В/Ч                                                                                                                      
  8   ВРЕМЕННОЕ УДОСТОВЕРЕНИЕ ЛИЧНОСТИ                                                                                                 
  9   СПРАВКА                                                                                                                          
  10  НАЦИОНАЛЬНЫЙ ПАСПОРТ                                                                                                             
  11  ПАСПОРТ СССР                                                                                                                     
  12  РАЗРЕШЕНИЕ НА ВРЕМ. ПРОЖИВАНИЕ   
  */
};

exports.GetPassReasons = function()
{
  var arrData = [
    { element   : '9038',
      value     : 'Достижение 45 лет',
      DB        : '7' },  

    { element   : '9037',
      value     : 'Достижение 20 лет',
      DB        : '6' }, 

    { element   : '9035',
      value     : 'Утрата паспорта',
      DB        : '10' },  

    { element   : '9041',
      value     : 'Обнаружение ошибок',
      DB        : '40' }, 

    { element   : '9039',
      value     : 'Изменение установочных данных',
      DB        : '3' }, 

    { element   : '9034',
      value     : 'Достижение 14 лет',
      DB        : '8' },  

    { element   : '9036',
      value     : 'Приобретение гражданства РФ',
      DB        : '15' }, 

    { element   : '9045',
      value     : 'Непригодность к использованию',
      DB        : '18' },  

    { element   : '9042',
      value     : 'Иное',
      DB        : '4' },  

    { element   : '9043',
      value     : 'Обмен паспорта СССР',
      DB        : '5' },  

    { element   : '9044',
      value     : 'Похищение паспорта',
      DB        : '33' },  

    { element   : '9048',
      value     : 'Прибытие из-за границы',
      DB        : '36' },

    { element   : '2070',
      value     : 'Смерть или объявление решением суда умершим (основание - свидетельство о смерти, оформленное в установленном порядке)',
      DB        : '11' },

    { element   : '2067',
      value     : 'Призыв на срочную военную службу',
      DB        : '23' },
	  
	{ element	: '2066',
	  value		: 'Другая причина',
	  DB		: '41' }
  ];
  return arrData;
  /*

Изменение установочных данных 9039
Утрата паспорта 9035
Достижение 20 лет 9037
Достижение 14 лет 9034
Достижение 45 лет 9038
Непригодность к использованию 9045
Обнаружение ошибок 9041
Иное 9042
Приобретение гражданства РФ 9036
Похищение паспорта 9044
Обмен паспорта СССР 9043
Прибытие из-за границы 9048


  12  В ДРУГОЙ РЕГИОН                                                                                                                  
  13  В МЕСТА ЛИШЕНИЯ СВОБОДЫ                                                                                                          
  23  В РОССИЙСКУЮ АРМИЮ                                                                                                               
  39  ВЫДАЧА ПАСПОРТА ВОЕННОСЛУЖАЩЕМУ                                                                                                  
  8   ДОСТИЖЕНИЕ 14 ЛЕТ
  6   ДОСТИЖЕНИЕ 20 ЛЕТ
  7   ДОСТИЖЕНИЕ 45 ЛЕТ
  25  ДРУГАЯ НУМЕРАЦИЯ
  4   ДРУГИЕ ПРИЧИНЫ
  34  ИЗМЕНЕНИЕ ПОЛА(ПРИБЫТИЕ)
  35  ИЗМЕНЕНИЕ ПОЛА(УБЫТИЕ)
  3   ИЗМЕНЕНИЕ(ПРИБЫТИЕ)УСТАНОВОЧНЫХ ДАННЫХ
  27  ИЗМЕНЕНИЕ(УБЫТИЕ)УСТАНОВОЧНЫХ ДАННЫХ
  18  НЕПРИГОДНОСТЬ ПАСПОРТА К ИСПОЛЬЗОВАНИЮ
  5   ОБМЕН ПАСПОРТА СССР
  40  ОБНАРУЖЕНИЕ ОШИБОК В ПАСПОРТЕ
  2   ПЕРЕЕЗД ПРИБЫТИЕ В ТОМ ЖЕ НАСЕЛЕННОМ ПУНКТЕ
  22  ПЕРЕЕЗД (ПРИБЫТИЕ) ВНУТРИ ОБЛАСТИ
  17  ПЕРЕЕЗД УБЫТИЕ В ТОМ ЖЕ НАСЕЛЕННОМ ПУНКТЕ
  28  ПЕРЕЕЗД (УБЫТИЕ) ВНУТРИ ОБЛАСТИ
  24  ПЕРЕИМЕНОВАНИЕ УЛИЦЫ
  16  ПЕРЕРЕГИСТРАЦИЯ В ЖИЛОЙ ФОНД
  31  ПЕРЕРЕГИСТРАЦИЯ С ВРЕМ. НА ПОСТ. МЕСТО ЖИТ.
  14  ПО РЕШЕНИЮ СУДА
  11  ПО СМЕРТИ
  21  ПО УВОЛЬНЕНИЮ В ЗАПАС
  30  ПОЛУЧЕНИЕ ВИДА НА ЖИТЕЛЬСТВО
  26  ПОЛУЧЕНИЕ РАЗРЕШЕНИЯ НА ВРЕМЕННОЕ ПРОЖИВАНИЕ
  33  ПОХИЩЕНИЕ ПАСПОРТА
  1   ПРИБЫТИЕ ИЗ ДРУГОГО РЕГИОНА
  20  ПРИБЫТИЕ ИЗ МЕСТ ЛИШЕНИЯ СВОБОДЫ
  36  ПРИБЫТИЕ ИЗ-ЗА ГРАНИЦЫ
  15  ПРИОБРЕТЕНИЕ ГРАЖДАНСТВА РФ
  38  РЕГИСТРАЦИЯ ПО МЕСТУ ПРЕБЫВАНИЯ
  9   РЕГИСТРАЦИЯ РЕБЕНКА
  19  САМОСТОЯТЕЛЬНАЯ РЕГИСТРАЦИЯ РЕБЕНКА
  29  УБЫТИЕ ДРУГИЕ ПРИЧИНЫ
  37  УБЫТИЕ ЗА ГРАНИЦУ
  41  УБЫТИЕ С МЕСТА ПРЕБЫВАНИЯ ДО ИСТЕЧЕНИЯ ЗАЯВЛЕННОГО СРОКА
  10  УТРАТА ПАСПОРТА
  
  42  ПРИБЫТИЕ
  43  УБЫТИЕ
  
  */
};
