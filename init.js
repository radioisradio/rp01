function Init()
{
	// подгружаем модуль анзиппера
	var unziper = require('./unziper.js');
	// распаковываем 
	unziper.UnzipData();	
	

	// подгружаем модуль парсера
	var parser = require('./parser.js');	
	parser.GetXmlData();
	
}

function ConvertToWin(strString) {

	var 
		iconv		= require('iconv-lite'),
		strOutput	= iconv.decode(strString, 'win1251');
	if (strOutput) {
		return strOutput;
	}
	else {
		return '';
	}

}



//Init();
//console.log(ConvertToWin('Тест'));
